package br.com.ldrson.tutorialsqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by leanderson on 12/09/16.
 */
public class PaisItemTable {

    private static String TABLE = "PAIS_ITEM";

    private static String COLUMN_ID = "ID";
    private static String COLUMN_NOME = "NOME";
    private static String COLUMN_CONTINENTE = "CONTINENTE";
    private static String COLUMN_URL_DA_BANDEIRA = "URL_DA_BANDEIRA";

    private static String[] ALL_COLUMNS = {COLUMN_ID,COLUMN_NOME,COLUMN_CONTINENTE,COLUMN_URL_DA_BANDEIRA};

    public static String CREATE_TABLE = "CREATE TABLE " + TABLE + "("
            + COLUMN_ID               + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_NOME             + " TEXT, "
            + COLUMN_CONTINENTE       + " TEXT, "
            + COLUMN_URL_DA_BANDEIRA  + " TEXT "
            +");";

    private SQLiteDatabase mDataBase;
    private DataBaseHelper mDataHelper;

    public PaisItemTable(Context context) throws SQLException {
        mDataHelper = DataBaseHelper.getInstance(context);
        open();
    }

    private void open() throws SQLException {
        mDataBase = mDataHelper.getWritableDatabase();
    }

    public long inserir(PaisItem paisItem){
        return mDataBase.insert(TABLE, null, toContent(paisItem));
    }

    public List<PaisItem> obterTodos(){
        Cursor cursor = null;
        List<PaisItem> list = new ArrayList<>();
        try {
            cursor = mDataBase.query(true,TABLE,ALL_COLUMNS,null,null,null,null,null,null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    list.add(toObject(cursor));
                } while (cursor.moveToNext());
            }
        }finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return list;
    }

    private PaisItem toObject(Cursor cursor) {
        PaisItem u = new PaisItem();
        u.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
        u.setNome(cursor.getString(cursor.getColumnIndex(COLUMN_NOME)));
        u.setContinente(cursor.getString(cursor.getColumnIndex(COLUMN_CONTINENTE)));
        u.setUrlDaImagemDaBandeira(cursor.getString(cursor.getColumnIndex(COLUMN_URL_DA_BANDEIRA)));
        return u;
    }

    private ContentValues toContent(PaisItem item) {
        ContentValues content = new ContentValues();
        content.put(COLUMN_NOME, item.getNome());
        content.put(COLUMN_CONTINENTE, item.getContinente());
        content.put(COLUMN_URL_DA_BANDEIRA, item.getUrlDaImagemDaBandeira());
        return content;
    }



}
