package br.com.ldrson.tutorialsqlite;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by leanderson on 02/09/16.
 */
public class PaisItemViewHolder extends RecyclerView.ViewHolder {

    public ImageView imageBandeiraDoPais;
    public TextView textNomeDoPais;
    public TextView textContinenteDoPais;

    public PaisItemViewHolder(View itemView) {
        super(itemView);
        imageBandeiraDoPais = (ImageView) itemView.findViewById(R.id.image_pais_bandeira);
        textNomeDoPais = (TextView) itemView.findViewById(R.id.text_pais_nome);
        textContinenteDoPais = (TextView) itemView.findViewById(R.id.text_pais_continente);
    }

}
