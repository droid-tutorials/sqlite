package br.com.ldrson.tutorialsqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private PaisItemRecyclerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new PaisItemRecyclerAdapter(this,obterListaDePaises());

        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        onItemClicado(position);
                    }
                })
        );

        DataBaseHelper.getInstance(this);

        findViewById(R.id.inserir_dados).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inserirDados();
                atualizarLista();
            }
        });

    }

    private void onItemClicado(int position){
        Toast.makeText(this,"Cliquei no item "+mAdapter.getItem(position).getNome(),Toast.LENGTH_SHORT).show();
    }

    private List<PaisItem> obterListaDePaises(){
        PaisItemTable table = new PaisItemTable(this);
        return table.obterTodos();
    }


    private void inserirDados(){
        List<PaisItem> lista = new ArrayList<>();

        PaisItem p = new PaisItem();
        p.setNome("Inglaterra");
        p.setContinente("Europa");
        p.setUrlDaImagemDaBandeira("http://famouswonders.com/wp-content/uploads/2009/11/England-flag.jpg");

        lista.add(p);

        p = new PaisItem();
        p.setNome("Escócia");
        p.setContinente("Europa");
        p.setUrlDaImagemDaBandeira("https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Flag_of_Scotland.svg/800px-Flag_of_Scotland.svg.png");

        lista.add(p);

        p = new PaisItem();
        p.setNome("País de Gales");
        p.setContinente("Europa");
        p.setUrlDaImagemDaBandeira("http://1.bp.blogspot.com/-s974oryDuQI/TaCw6DD2_KI/AAAAAAAAACs/fnK8by8GBBc/s1600/Wales_Flag.jpg");

        lista.add(p);

        p = new PaisItem();
        p.setNome("Irlanda");
        p.setContinente("Europa");
        p.setUrlDaImagemDaBandeira("https://upload.wikimedia.org/wikipedia/commons/1/13/Ireland_flag_300.png");

        lista.add(p);

        PaisItemTable table =new PaisItemTable(this);
        for(PaisItem item :lista){
            table.inserir(item);
        }

    }

    private void atualizarLista(){
        mAdapter.clear();
        mAdapter.addAll(obterListaDePaises());
        mAdapter.notifyDataSetChanged();
    }

}
